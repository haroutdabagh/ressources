---
title:  'Express and Unit Testing'
author:
- Jaya Nilakantan, Patricia Campbell
- 420-520-DW
revealjs-url: revealjs
---
<style>
  ol li {
    list-style-type: lower-alpha
  }
  .present p, .present li {
    text-align: left;
  }
</style>

## Recall - Express.js

```js
import express from 'express';
const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
```

## With CJS

```js
const express = require('express');
//everything else is the same
```

## Using a router

* it is useful to group common routes together
* allows better modularization of your code

## Example

* routes/wiki.js
      * contains all the named function for the routes
      * [Example source 1](https://fek.io/blog/how-to-add-unit-testing-to-express-using-jest) [and 2](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/routes)

```js
function index(req, res) {
  res.send("Wiki home page");
}
function about(req, res) {
  res.send("About this wiki");
}
export  {  index,  about } 
```

---

* routes/wikirouter.js
    * defines the Router middleware
    * you could put the route code here too, if it is simple

```js
import express from 'express';
const router = express.Router();
//deconstructing assignment
import  {index, about} from "./wiki.js";
// add routes to the Router object
// Home page route.
router.get("/", index);
// About page route.
router.get("/about", about);
export  router;
```

---

* app.js
    * main application file
```js
import express from 'express';
const app = express();
import wiki from  "./routes/wikirouter.js";
// anything in the URL path /wiki uses the Router
app.use("/wiki", wiki);
///other routes
export app;
```

## Unit testing - simple

```js
function index(req, res) {
    res.send('Wiki home page');
}
```
* don't care about `req`
* need to make sure `res.send` is invoked with the correct argument
* we don't want to rely on the actual Express `req` and `res` objects - we want to unit test, i.e., test in isolation

## Mocking

```js
import  { index } = from "../routes/wiki";
test("responds to /", () => {
  //generic mock object
  //res must have a send function
  const res = { 
    text: "",
    send: function(input) {
      this.text = input 
    } 
  };
  const req = {};
  index(req, res);   
  expect(res.text).toEqual("Wiki home page");
});
```

---

* it is easy to create basic mock objects of your own when the test cases have no dependancies

## Testing with dependancies

* let's say that your function uses [axios](https://axios-http.com/docs/intro) to make an http request to another site
  * Fetch API was not supported natively by node.js before 17.5
  * we often want our server to query another endpoint
    * e.g., authentication is required, we don't want to share our credentials with the browser client
[Example source](https://rodgobbi.com/mock-module-jest/)
---

```js
// api/users.js
import axios from 'axios';

const getUserData = async (id) => {
  try {
    const resp = await axios.get(`/api/user/${id}`);
    return resp.data;
  } catch (error){
    console.error(error);
    throw error;
  }
};
export  getUserData;
```

## Mocking axios module

* to unit test `getUserData` we need to mock `axios`
* in `__tests__/api/users.tests.js`
```js
//need to require dependancies
import axios from 'axios';
import getUserData  from '../../api/users.js';
//tell jest that both test case and user.js code will use mock instead of real axios module
jest.mock('axios');

// make sure you reset if you have multiple test cases in the suite, since
// they all share the same mock
beforeEach(() => {
  axios.get.mockReset();
});
//test suite
describe('getUserData', () => {
  //test case
  test('should call the API and return the data', async () => {
    const userId = 51;
    const userData = { id: userId, name: 'Allan' };
    axios.get.mockResolvedValue({ data: userData });

    const returnedUserData = await getUserData(userId);

    expect(returnedUserData).toEqual(userData);
  });
});
```

## Mocking an entire module

* when we mock the entire module, the function we are testing will not  use the module and will use the mock module instead
* this makes our test cases reproducible - no reliance on an external dependancy
* after you mock the module, you can provide:
  * `mockReturnValue(value)` when the mock function returns
  * `mockImplementation(() => throw new Error())` when mocking an exception
  * `mockResolvedValue` or `mockRejectedValue(new Error())` when the module returns a Promise

## Alternative

```js
//need to require dependancies
import axios from 'axios';
import getUserData from '../../api/users.js' ;

const userId = 51;
const userData = { id: userId, name: 'Allan' };
describe('getUserData', () => {
  //for all test cases in this suite
  beforeEach(() => {
    //only mocking the get function and only here
    axios.get = jest.fn().mockResolvedValue({ data: userData });
    )
  });
  //test case
  test('should call the API and return the data', async () => {
    const returnedUserData = await getUserData(userId);
    expect(returnedUserData).toEqual(userData);
  });
});
```

## Mocking a function

* `jest.fn()` returns a mock function, with the implementation between the brackets
* this allows you to mock specific functions only

## Testing Express routes

[Resource](https://www.albertgao.xyz/2017/05/24/how-to-test-expressjs-with-jest-and-supertest/)

* when we are testing routes, we need to ensure that our server doesn't start listening on the port
* so we separate the server and the main application routes into different files

---

```js
//app.js
import express from 'express';
const app = express();
///Routes are here either directly or using a router
export  app;
```

---

```js
//the application entry point is the JavaScript file /bin/www
// (this is what express-generator calls it)
//bin folder is where start-up scripts are defined
#!/usr/bin/env node
import app from "./app.js"

app.listen(3000, () => {
  console.log("Server listening on port 3000!");
});
```

## Testing routes with http 

* now that the server is separated, we will mock the http server with `supertest`

`npm install --save-dev supertest`

## Testing a route
[source](https://www.rithmschool.com/courses/intermediate-node-express/api-tests-with-jest)
```js
//app2.js
import express from "express";
const app = express();

const teachers = ["Jaya", "Tricia", "Swetha", "Dirk"];

app.get("/", (req, res) => {
  return res.json(teachers);
});

export app;
```

---

```js
// we will use supertest to test HTTP requests/responses
import request from "supertest";
// we also need our app for the correct routes!
import app from "../app2.js"

describe("GET / ", () => {
  test("It should respond with an array of teachers", async () => {
    const response = await request(app).get("/");
    expect(response.body).toEqual(["Jaya", "Tricia", "Swetha", "Dirk"]);
    expect(response.statusCode).toBe(200);
  });
});
```

