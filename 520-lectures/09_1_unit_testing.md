---
title:  'Unit test with Jest'
author:
- Jaya Nilakantan, Patricia Camplbell
- 420-520-DW
revealjs-url: revealjs
---
<style>
  ol li {
    list-style-type: lower-alpha
  }
  .present p, .present li {
    text-align: left;
  }
</style>

## Unit test?
[source](https://cs.gmu.edu/~tlatoza/teaching/swe432f19/Lecture-4-Tooling-and-Testing.pdf)

Unit testing is testing an component/unit in isolation

Testing in isolation means it is much easier to debug

Programmers are responsible for unit testing during implementation

Many programmers write test cases first - it's a good way to make sure you understand the requirements

## Integration testing?

* Components might work in isolation, but might not work together:
  * API was not understood the same way by both developers
  * issues with side effects that were not tested and unexpected
  * timing issues (e.g. `async` function not `await`ed)
  * many more reasons why code breaks!
* integration test redo unit tests that previously used a *stub* to stand in for the other unit

## Writing code that is testable

* starting from test cases (how do you prove your code works) before coding will change the way you code!
* some code is hard to unit test if it has many dependancies
  * but can be tested if you change your approach
  * leads to much more robust code

__Note__ Sometimes called Test Driven Design TDD

## Example 1

```js
const accounts = {
  a: 100,
  b: 0,
  c: 20
};

function getAccount() {
  let accountName = prompt("Enter an account name");
  if (!accounts.hasOwnProperty(accountName)) {
    throw new Error("no account found");
  }
  return accountName;
}

function transfer(from, amount) {
  if (accounts[from] < amount) {
    return;
  }
  accounts[from] -= amount;
  accounts[getAccount()] += amount;
}
```
[Example inspiration](https://eloquentjavascript.net/08_error.html)

---

* what could go wrong?

---

 * `transfer` first removes the money from the account and then calls `getAccount` before it adds it to another account. 
 * if it is broken off by an exception at that point, it’ll just make the money disappear.
 * in this case, it is staightforward, and you will change the order of steps
 * let's look at how to reduce side effects (e.g., from the error handling changing the flow)

## Reducing side effects

* change the order
* think of the steps as a transaction (remember from your database courses?)
  * compute new intermediate values
  * change existing values only when all steps are complete

---

```js
function transfer(from, amount) {
  if (accounts[from] < amount) {
    return;
  }
  let step = 0;
  try {
    accounts[from] -= amount;
    step = 1;
    accounts[getAccount()] += amount;
    step = 2;
  } finally {
    //abort the transaction
    if (step == 1) {
      accounts[from] += amount;
    }
  }
}
```

## Unit coding transfer

* `transfer` has a dependancy on `getAccount`
* unit testing must test `transfer` in isolation
  * therefore `getAccount` needs to be "stubbed"
  * we pass the `transfer` function to `getAccount` so that we can test it by passing another test stub
  * called dependency injection

---

```js
const accounts = {
  a: 100,
  b: 0,
  c: 20
};
function getAccount() {
  let accountName = prompt("Enter an account name");
  if (!accounts.hasOwnProperty(accountName)) {
    throw new Error("no account found");
  }
  return accountName;
}
function transfer(from, amount, retrieveAccount) {
  if (accounts[from] < amount) {
    return;
  }
  accounts[from] -= amount;
  accounts[retrieveAccount()] += amount;
}
```

---

* test with different hardcoded `retrieveAccount` functions to test all possible paths
* e.g.:
  * `return "b";`
  * throw new Error("no account found");`

## Example 2

```js
let y = 0;
function move(){
  const element = document.getElementById("image");
  if (y == 0){
    y = 100;
    element.style.transform = `translateY(${y}px)`;
  }
}
```
* how do you know if `move` did what it was supposed to do?
* `y` is a shared state: there can be unexpected bugs if anyone else changes `y`

---

```js
function move(y, image){
  if (y == 0){
    image.style.transform = "translateY(100px)";
    return 100;
  } else {
    return y;
  }
}
```

---

![side effects](img/functional.png)[source](https://navdeepsinghh.medium.com/basic-building-blocks-of-functional-reactive-programming-frp-ios-4b31665b6ff1)

---

![xkcd side effects](img/hero.png)[source](https://www.explainxkcd.com/wiki/index.php/1790:_Sad)

## Figuring out unit tests

* looks at the specs - make sure you understand the expected behaviour
* can you describe what should happen?
* can you say what you expect under certain test scenarios?

---

Test-driven development -> write test cases first based on the specs

* test cases become the documentation!
* test cases written before you code!

## Jest

* `jestjs.io` originally rose to prominence for testing React components
* built by Facebook 😑
* now a top JS testing framework (along with mocha/chai and jasmine)
  * these JS test frameworks have similar syntax so it will be easy to move from one to another
* can use Jest for front-end _and_ back-end testing

## Installation

* a testing framework is not needed for production code so we install it for development only

```
npm init
npm install --save-dev jest
```

* open `package.json` and:
  * change script `test` value to `node --experimental-vm-modules node_modules/jest/bin/jest.js` **if you are using ESM**
  * add the pair `"type": "module",` to indicate we are using modules
* write test cases in a folder named `__tests__`
* run test cases with `npm run test`

## Our Express app directory structure

* we will put all files that will be sent to the browser (e.g., css, js, images) in a folder called `public`
* all business logic in a folder call `controllers` 
* all test files in `__tests__`

## Example

File `controllers/sum.js`:

```js
function sum(a, b) {
  return a + b;
}
export default sum;
```

File`__tests__/sum.test.js`
```js
const sum = require('../api/sum.js');

test('adds two positive numbers', () => {
  expect(sum(1, 2)).toBe(3);
});
```
[source](https://jestjs.io/docs/getting-started)

## Warning 1 ⚠

* Jest support for ESM is still "experimental".
* Jest looks for `.js` files, so name appropriately.
* make sure you added `  "type": "module",` to your package.json
* the latest `.eslintrc.json` includes `controllers` folder as containing modules

## Warning 2 ⚠

* doesn't support networked drives (H: drive) - you will have to work on the desktop in the labs
  * you will see the error `Error: EPERM: operation not permitted, lstat '\\fileserver\home\'` if you forget
* ALWAYS push your work to git!!

## Running jest test cases

* `jest` only if installed globally and added to PATH (not in the labs)
  * If you want jest to keep running and monitoring for file changes: `jest --watch`
* `npm run test` if installed for the project

## Jest key functions

* `test` for a test case
  * first argument is the description of the test case
  * second is a callback, usually taking no parameters

## expect

* the `expect` function is used to validate actual result with expected result with [matchers](https://jestjs.io/docs/expect)
  * `.toBe` -> exact equality (like `==` in Java)
  * `.toEqual` -> value equality (like deep `.equals`)
  * `.not` -> to negate a matcher
  * `.toBeGreaterThan`, ...
  * `.toBeNull()`, `.toBeUndefined()`
  * `.toThrow()`

*many* more examples!


## Test driven development

1. write test cases - they will fail since no code to test!
2. then write code
3. when that test passes, add more test cases
4. write code
5. **refactor code**

## Exercise

Write and test a module that exports a function `letterCount` that takes a character and a string, and returns the number of occurences

1. write a test case in `__tests__/count.test.js`
2. run it - the testcase will fail
3. add the function header and export in `controllers/count.js`
4. keep adding code until the test case passes
5. add more test cases for **edge** cases: uppercase/lowercase characters, special characters, invalid input etc...)

[source](https://www.rithmschool.com/courses/intermediate-node-express/api-tests-with-jest)

## What about an exception?
Let's say you expect `letterCount` to throw an expection if either argument's `typeof` is not a string

---
```js
const letterCount = require("../controllers/count.js");

test("letterCount with number", () => {
  expect(() => {
    letterCount(7, 100);
  }).toThrow();
});
```
Note: You must wrap the code in a function, otherwise the error will not be caught and the assertion will fail.

## Unit testing an async function

What will happen if `letterCount` is `async` and your test case says:

```js
test("letterCount with regular strings", () => {
  expect(letterCount("a", "Jaya")).toBe(2);
});
```

## Solution

```js
test("letterCount with regular strings", async () => {
  const count = await letterCount("a", "Jaya");
  expect(count).toBe(2);
});
```

## Alternative syntax for resolution

```js
test("letterCount with regular strings", async () => {
  await expect(letterCount("a", "Jaya")).resolves.toBe(2);
});
```

## What about catching a rejection?

```js
test('letterCount with number', async () => {
  expect.hasAssertions();
  try {
    await letterCount(7, 100);
  } catch (e) {
    expect(e).toMatch('error');
  }
});
```

* `expect.hasAssertions()` verifies that at least one assertion is called during a test. This is useful when testing asynchronous code, in order to make sure that assertions in a callback actually got called.

## Alternative syntax for rejection
```js
test("letterCount with number", async () => {
  await expect(letterCount(7, 100)).rejects.toThrow();
});
```

