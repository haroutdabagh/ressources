---
title:  'React - Part 1'
author:
- Jaya Nilakantan, Patricia Campbell
- 420-520-DW
revealjs-url: revealjs
---
<style>
  ol li {
    list-style-type: lower-alpha
  }
  .present p, .present li {
    text-align: left;
  }
</style>

## Front-end frameworks

Goals:

* user experience - mobile friendly - responsive
* scalable site
* maintainable code
* rapid development

## React particulars

* joins together content (HTML) and behaviour (JS) into Components
    * components are reusable
    * components achieve separation of concerns based on functionality 
    * components are coded with JSX (JavaScript XML)
* virtual DOM
    * changes are made to the virtual DOM
    * only the differences are rendered to the DOM
    * goal: to make DOM updates as fast as possible

## Components

* UI is seen in terms of components
  * components can contain components
  * components are initialized with properties `props`
  * components are stateful `state`
  * components have a lifecycle: 
    * `constructor`
    * `render`
    * `componentDidMount`
    * `shouldComponentUpdate`
    * `componentDidUpdate`
    * `componentWillUnmount`
    * `componentDidCatch` -error handling

---

![image](https://projects.wojtekmaj.pl/react-lifecycle-methods-diagram/ogimage.png) [React Lifecycle](https://projects.wojtekmaj.pl/react-lifecycle-methods-diagram/)

## Data

* pass data to the top-level component to its children through `props`
  * `props` is an object that contains the properties that are set in the constructor and don’t change
* `state` is used for data that changes over time 
   * managed within the component
   * state change through `setState` will automatically re-render the component to the virtual DOM

## Setup

* we will be using [create-react-app](https://create-react-app.dev/) in this course to bootstrap our React app
* the modules will be installed by CRA, and the build tools will be configured for us - webpack, babel

---

From the root of the project **not H drive**, if you want the React application in a folder called client, run:
```
npx create-react-app client
cd client
npm start
```

* Notice the `npm start` command in the `package.json`. 
```
$ cd client/
$ grep start package.json
    "start": "react-scripts start",
```
This will start the development server using a CRA helper `react-scripts`.

## Directory structure

```
client
├── README.md
├── node_modules
├── package.json
├── .gitignore
├── public
│   ├── favicon.ico
│   ├── index.html
│   ├── logo192.png
│   ├── logo512.png
│   ├── manifest.json
│   └── robots.txt
└── src
    ├── App.css
    ├── App.js
    ├── App.test.js
    ├── index.css
    ├── index.js
    ├── logo.svg
    ├── reportWebVitals.js
    └── setupTests.js
```

---

## public and build folder

* when the client project is built for production, the minified bundle created by `webpack` will be placed in the `/build` folder
* the `/build` folder is not part of the repository, and is not tracked by git 
* the `/build` folder is where the Express server wil serve static files: but during development, you can start the client and the server separately:
  1. server: start with node to listen to requests
  1. client: build and start with Live Server

## Changes in the public folder

* index.html - common meta data, title, favicon, etc.
* contains a `div` with the `id` of `root`

## src folder

The `src` folder contains the React components. 

* `index.js` is the entry point for the React app. It will place the `App` component into the `root` div.
* notice the `App` component is imported from `./App.js`
* also notice that each component can have its own `.css` file which is also imported!
* `webpack` will automatically bundle the component and their `.css` file into a bundle

## App.js

* The `App` component is the top-level component of the React app.
* It uses the function syntax of a component: the `return` value is what will be rendered to the virtual DOM.
* keep it simple, it should just render a reusable component
* App is not really reusable given its name! 

## Example - Sushi card

* Write a component called `SushiCard` that displays a info about a sushi.
* The component accepts 3 `prop`s:  the alt text, description and imageURL.
* add `SushiCard.css` to the `component` folder for the specific styling

## Changes to App

* `App` will create a single `SushiCard` component
* import SushiCard from `./components/SushiCard`
* remove the `header`, and replace with a `SushiCard` component

## To run the app

`npm start`

* development server will start, listening on port 3000

---

**BEWARE** React does **NOT** run on the server. It runs on the client.

* However, in development mode, a React app needs a development server to build the JSX files. The browser does not understand JSX, so the development server is required.
* In 320, you may have used another mechanism to have the JSX transpiled to JS, for pedagogical reasons.

## Example - Interactivity

The SushiCard component is useful to create a bunch of cards, but there is no interactivity (e.g., change in response to user or any other event)

* we will create a new component called `Counter` that will display a counter and increment it when the user clicks on it
* the `Counter` component will accept an `initialCount` prop, which will be the initial value of the counter

---

* since we want to increment the counter, we will need to add a `handleClick` method to the `Counter` component
* the `handleClick` method will **change the state** of the component, which will cause the component to re-render

## component/Counter.js and App.js

## Important notes!

* props are **immutable**: a component must never modify its own props. 
* However, the parent component can re-render a child component with new props.

---

* state is **mutable**, but if you want to trigger a re-render, you need to call `setState`
  * e.g. `this.setState( {counter: 0} )`
* changing the `this.state` directly will not re-render the component, the only place where you should set `this.state` is in the `constructor`
* You don’t have to set every property of the state object; React will merge them (i.e., only overwrites what you are setting)

---

* React events are not the same as DOM inline event handlers. React uses a SyntheticEvent
* Be careful about how you pass event handlers to components (see [docs](https://reactjs.org/docs/faq-functions.html))

---

### Handlers Option 1 - Bind in the constructor

```js
<button onClick={this.handleClick}>
```

requires the constructor to *bind* `this` to the function for the function to access attributes of the component (e.g., `this.state`)

```js
this.handleClick = this.handleClick.bind(this);
```

--- 

* you only need to bind the methods you pass to other components so they know which object is represented by `this`. For example, `<button onClick={this.handleClick}>` passes `this.handleClick` to a `button` so you want to bind `this` to the component.

--- 

### Handlers Option 2 - Class property syntax

```
<button onClick={this.handleClick}>
```

and the `handleClick` **method** is:

```js
handleClick = () => {
    ...
  }
```

---

### Handlers Option 3 and 4 - Bind in the render method

```js
<button onClick={() => this.handleClick()}>
<button onClick={this.handleClick.bind(this)}>
```

* the arrow function is a shorthand for `this.handleClick.bind(this)` the statements are equivalent
* this is creating a new function every time the component renders, may have performance implications (but wait before optimizing)

---

Binding is a general JavaScript concept to attach a function to an object, not specific to React. Read more about it [here](https://yehudakatz.com/2011/08/11/understanding-javascript-function-invocation-and-this/)


## React Lifecycle

![image](https://projects.wojtekmaj.pl/react-lifecycle-methods-diagram/ogimage.png) [React Lifecycle](https://projects.wojtekmaj.pl/react-lifecycle-methods-diagram/)

[Documentation](https://reactjs.org/docs/react-component.html#the-component-lifecycle)

--- 

### Mounting

The mounting phase is when a component is being created and added to the DOM

  * constructor
  * render
  * componentDidMount

--- 

### Updating

An update is caused by changes to state or props

  * render
  * componentDidUpdate

The functions `setState` and `forceUpdate` are used to trigger an update

--- 

### Unmounting

The unmounting phase is when a component is being removed from the DOM

  * componentWillUnmount

## constructor

* where you initialize state and bind handlers/callbacks to `this`
* the first line of the constructor is a call to `super(props)`
* don't call `setState` in the constructor, this is the <u>only place</u> where you should assign an object _directly_ to `this.state`

## render

* `render` is the function that returns the JSX to be rendered to the virtual DOM
* `render` is called every time the component is rendered
* it typically looks at the `props` and `state`, but should not modify state (it should be a pure function, with no side effects)

--- 

* Do not `setState` inside the render function directly. Why? `setState` causes the `render` function to be called again which can cause infinite recursion
* it should return a single element

## JSX syntax

* `{}` brace brackets to insert JS variables and expressions
* HTML attributes are in camelCase instead of `-`
* JS reserved words used in HTML/css have a different version, for example:
   * `class` is `className`
   * `for` is `htmlFor`
* tags must be closed (since it is XML) with closing tag or `/>`

## componentDidMount 

* `componentDidMount` is invoked immediately after a component is mounted (inserted into the tree). 
* **If you need to load data from a remote endpoint, this is a good place to fetch the network request.**
* You may call `setState()` immediately in componentDidMount(). `render()` will be called twice in this case, but the user won’t see the intermediate state since the browser will update the screen only once

## componentDidUpdate

*  `componentDidUpdate` is invoked immediately after updating occurs. This method is not called for the initial render.
* **This is also a good place to do network requests if the current props or state have changed** 
* You may call `setState()` immediately in `componentDidUpdate()` but beware of causing an infinite loop (what caused the component to update, and will it keep happening after you render?). 
  * To be safe, compare the current props to previous props, and only setState if they are changed, otherwise there will be an infinite loop

## Example - Quotes application

* Getting initial data from the server
  * parent component gets the quote
  * child gets the data as a prop

## Example - Getting updated data from the server

* button on child invokes callback function in parent
* parent gets the next quote, updates its state
* child component is given new props and will re-render

## componentDidUpdate

* if the props were not complete and the child need to fetch additional data, it would have to do so in the `componentDidMount` and the `componentDidUpdate` method
* to avoid unecessary fetch, we always check that the new props are different from the old ones with the `componentDidUpdate` method
* `componentDidUpdate` has access to the old props and state through the `prevProps` and `prevState` parameters
* check that the current props are different from the `prevProps` before re-rendering

---

* lets say the Quote component was in charge of getting the quote from the server based on a category in its props:

```js
  async componentDidUpdate(prevProps) {
    if (prevProps.category !== this.props.category) {
      await this.fetchQuote();
    }
  } 
```

## React Hooks

[React Hooks](https://reactjs.org/docs/hooks-intro.html)

* Hooks is a feature that allows you to use state and other React features without writing a class
* Hooks are a new feature in React 16.8.0 (current version is 18.2)
* There are two types of hooks:
  * **state hooks**
  * **effect hooks**

## Motivation

Hooks give you a way to extract common logic from components, allowing for easier reuse. Multiple components can reuse the same stateful logic, without sharing the same state.

You can think of Hooks as being the React answer to the Single Responsibility Principle.

## State Hooks

`useState()` hook  [codepen](https://codepen.io/TriciaProf/pen/RwLPZqv) 

```jsx
import React, { useState } from 'react';

function Example() {
  // Declare a new state variable, which we'll call "count"
  // arg1  count is init with the arg to useState()  0 in this case
  // arg2  setCount() name of the func that will update the state of count
  const [count, setCount] = useState(0);

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Click me
      </button>
    </div>
  );
}
```

---

`this.setState()`  explicit

```jsx
class Example extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0
    };
  }

  render() {
    return (
      <div>
        <p>You clicked {this.state.count} times</p>
        <button onClick={() => this.setState({ count: this.state.count + 1 })}>
          Click me
        </button>
      </div>
    );
  }
}
```

## Understanding State Hooks

* `useState` is a Hook function that lets you use state in a functional component
* the first time the component is created, `useState` returns an array of two values:
   * `const [count, setCount] = useState(0);`
   * a new state variable initialized to the value of the `useState` parameter
   * a function that lets you update the state
   * uses a JS feature called array destructuring to assign the values to the variables `count` and `setCount`
* can use `count` as a variable in the return statement

## Multiple state values?

No problem! You can use multiple state variables or put them altogether in a single object. Your choice! But remember that the goal of Hooks is to make your code more modular and reusable by focusing on stateful logic

```jsx
function ExampleWithManyStates() {
  // Declare multiple state variables!
  const [age, setAge] = useState(42);
  const [fruit, setFruit] = useState('banana');
  const [todos, setTodos] = useState([{ text: 'Learn Hooks' }]);
```

## Effect Hooks

state hooks allow us to use state in function components, so we don't have to write class component, but what about the lifecycle methods?

* Effect hooks are used to perform *side effects* in function components
   * like fetching data from the server, logging, reading files, etc.
   * `useEffect` hook is like `componentDidMount` and `componentDidUpdate` combined
   * it runs after the initial render and after every update

## Example

[codepen](https://codepen.io/TriciaProf/pen/OJEQYwr)

```jsx
function Example() {
  const [count, setCount] = useState(0);

  useEffect(() => {
    document.title = `You clicked ${count} times`;
  });

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Click me
      </button>
    </div>
  );
}
```

## Rewriting the Quotes app with Hooks

## Caution: useEffect

* remember that `useEffect` runs after *every* render
* it is easy to get caught in an infinite loop if the effect changes state
* solution - use the second parameter of `useEffect` to indicate what changes will cause the effect to run
```jsx
useEffect(() => {
        // do something
   }, [])
```
   * the empty array means that the effect will only run when the component is rendered and not when it is updated
   * if you want to run the effect every time the state changes, you can use the second parameter to indicate which state changes should cause the effect to run

[Documentation](https://reactjs.org/docs/hooks-effect.html)
